package br.ufsc.inf.lapesd.ontogenesis.adapter;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.function.Supplier;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

import org.apache.commons.io.IOUtils;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;

import com.google.gson.Gson;

import br.ufsc.inf.lapesd.alignator.core.entity.loader.ServiceDescription;
import br.ufsc.inf.lapesd.ontogenesis.adapter.config.OntoGenesisAdapterConfig;
import br.ufsc.inf.lapesd.ontogenesis.adapter.config.SimpleOntoGenesisAdapterConfig;
import br.ufsc.inf.lapesd.ontogenesis.engine.SemanticFeatures;

@Provider
public class OntoGenesisAdapter {
	
    @Nonnull
    private OntoGenesisAdapterConfig config;
    @Nonnull
    private Model ontology = ModelFactory.createDefaultModel();
    @Nullable
    private SemanticFeatures semanticFeatures;
    
    private boolean isRegistered = false;


    public OntoGenesisAdapter(@Nonnull OntoGenesisAdapterConfig config) {
        this.config = config;
        this.instance = this;
    }

    private static  OntoGenesisAdapter instance =
            new OntoGenesisAdapter(new SimpleOntoGenesisAdapterConfig());

    public static OntoGenesisAdapter getInstance() {
        return instance;
    }

    @Nonnull
    public OntoGenesisAdapterConfig getConfig() {
        return config;
    }

    public Model getOntology() {
        return ontology;
    }

    @Nullable
    public SemanticFeatures getSemanticFeatures() {
        return semanticFeatures;
    }

    /**
     * Registers this service in OntoGenesis
     */
    public void registerService() {
        try {
            ServiceDescription serviceDescription = getConfig().getDescription();

            Client client = createClient();
            WebTarget webTarget = client.target(config.getOntogenesisURI()).path("microservice/description");

            Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);

            String stringDescription = new Gson().toJson(serviceDescription);
            Response response = invocationBuilder.post(Entity.entity(stringDescription, MediaType.APPLICATION_JSON));

            int status = response.getStatus();

            if (status == 200) {
                System.out.println("--> Service successfully registered in OntoGenesis.");
                isRegistered = true;
                String readEntity = response.readEntity(String.class);
                setSemanticFeatures(new Gson().fromJson(readEntity, SemanticFeatures.class));
            }
            else {
                System.err.println("Fail to register this service in OntoGenesis: HTTP status "+status);
            }
        } catch (Throwable e) {
            System.err.println("Fail to register this service in OntoGenesis: "+e.getMessage());
            //e.printStackTrace();
        }
    }

    private Client createClient() {
        Supplier<Client> fac = config.getClientFactory();
        return fac == null ? ClientBuilder.newClient() : fac.get();
    }

    /**
     * Sends a intercepted representation to OntoGenesis
     * @param entity
     * @param mediaType
     * @return SemanticFeatures which contains the Ontology and Semantic Mappings
     */
    public SemanticFeatures sendToOntoGenesis(Object entity, MediaType mediaType) {
        try {
            ServiceDescription serviceDescription = config.getDescription();
            String serverPort = serviceDescription.getServerPort();
            String uriBase = serviceDescription.getUriBase();

            Client client = createClient();
            WebTarget webTarget = client.target(config.getOntogenesisURI())
                    .path("microservice/representation")
                    .queryParam("serverPort", serverPort)
                    .queryParam("uriBase", uriBase);

            Response response = webTarget.request(MediaType.APPLICATION_JSON)
                    .post(Entity.entity(entity, mediaType));

            int status = response.getStatus();
            if (status == 200) {
                String readEntity = response.readEntity(String.class);
                setSemanticFeatures(new Gson().fromJson(readEntity, SemanticFeatures.class));
                return semanticFeatures;
            }
            else {
                System.err.println("Fail to send representation to OntoGenesis: HTTP status "+status);
            }
        } catch (Throwable e) {
            System.err.println("Fail to send representation to OntoGenesis: "+e.getMessage());
            //e.printStackTrace();
        }
        return null;
    }

    public void setSemanticFeatures(@Nullable SemanticFeatures semanticFeatures) {
        this.semanticFeatures = semanticFeatures;
        if (semanticFeatures == null) {
            if (!this.ontology.isEmpty())
                this.ontology = ModelFactory.createDefaultModel();
        } else {
            Model ontology = ModelFactory.createDefaultModel();
            String owl = semanticFeatures.getOntologyAsString();
            try (InputStream in = IOUtils.toInputStream(owl, StandardCharsets.UTF_8)) {
                RDFDataMgr.read(ontology, in, Lang.RDFXML);
                this.ontology = ontology;
            } 
            catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }
    
    public boolean isRegistered() {
    	return this.isRegistered;
    }

}
