package br.ufsc.inf.lapesd.ontogenesis.adapter.config;

import java.util.function.Predicate;
import java.util.regex.Pattern;

public class MultiPatternPredicate implements Predicate<String> {
    private Pattern pattern = null;

    public MultiPatternPredicate addRegex(String regex) {
        String rx = "";
        if (pattern != null) rx = this.pattern.pattern() + "|";
        rx += "(?:" + regex + ")";
        pattern = Pattern.compile(rx);
        return this;
    }

    @Override
    public boolean test(String s) {
        return pattern != null && pattern.matcher(s).matches();
    }
}
