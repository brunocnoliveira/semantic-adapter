package br.ufsc.inf.lapesd.ontogenesis.adapter.config;

import br.ufsc.inf.lapesd.alignator.core.entity.loader.ServiceDescription;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.ws.rs.client.Client;
import java.util.function.Predicate;
import java.util.function.Supplier;

public interface OntoGenesisAdapterConfig {
    @Nonnull String getServiceBaseURI();
    /**
     * Gets the ontology resource path relative to the service base URI. 
     * 
     * Must not include a leading slash
     */
    @Nonnull String getOntologyResourcePath();
    @Nonnull String getOntogenesisURI();
    @Nonnull ServiceDescription getDescription();
    @Nullable Supplier<Client> getClientFactory();
    @Nonnull Predicate<String> getResponseFilterIgnore();

    void setServiceBaseURI(@Nonnull String value);
    void setOntologyResourcePath(@Nonnull String value);
    void setOntogenesisURI(@Nonnull String value);
    void setDescription(@Nonnull ServiceDescription description);
    void setClientFactory(Supplier<Client> factory);
    void setResponseFilterIgnore(@Nonnull Predicate<String> predicate);
}
