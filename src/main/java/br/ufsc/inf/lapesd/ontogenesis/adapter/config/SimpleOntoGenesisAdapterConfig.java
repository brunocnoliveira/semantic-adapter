package br.ufsc.inf.lapesd.ontogenesis.adapter.config;

import br.ufsc.inf.lapesd.alignator.core.entity.loader.ServiceDescription;

import javax.annotation.Nonnull;
import javax.ws.rs.client.Client;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class SimpleOntoGenesisAdapterConfig implements OntoGenesisAdapterConfig {
    private String serviceBaseURI = null;
    private String ontologyResourcePath = "/ontology";
    private String ontogenesisURI = null;
    private ServiceDescription description;
    private Supplier<Client> clientFactory = null;
    private Predicate<String> responseFilterIgnore = s -> false;
    //private boolean ldAsJSON = false;

    @Nonnull
    public String getServiceBaseURI() {
        if (serviceBaseURI != null)
            return serviceBaseURI;
        String uri = getDescription().getIpAddress();
        if (!uri.matches("https?://.*")) {
            /* uri is actually an ip */
            String portString = getDescription().getServerPort();
            int port =  portString == null ? 80 : Integer.parseInt(portString);
            String pathBase = getDescription().getUriBase();
            pathBase = pathBase == null ? "" : pathBase;
            uri = (port == 443 ? "https://" : "http://") + uri
                    + (port != 80 && port != 443 ? ":" + port : "") + pathBase;
        }
        return uri;
    }

    public void setServiceBaseURI(@Nonnull String serviceBaseURI) {
        this.serviceBaseURI = serviceBaseURI;
    }

    @Nonnull
    public String getOntologyResourcePath() {
        return ontologyResourcePath;
    }

    public void setOntologyResourcePath(@Nonnull String ontologyResourcePath) {
    	if (ontologyResourcePath.startsWith("/")) 
    		ontologyResourcePath = ontologyResourcePath.substring(1);
        this.ontologyResourcePath = ontologyResourcePath;
    }

    @Nonnull
    public String getOntogenesisURI() {
        return ontogenesisURI;
    }

    public void setOntogenesisURI(@Nonnull String ontogenesisHost) {
        this.ontogenesisURI = ontogenesisHost;
    }

    @Nonnull
    public ServiceDescription getDescription() {
        return description;
    }

    @Override
    public Supplier<Client> getClientFactory() {
        return clientFactory;
    }

    @Nonnull
    @Override
    public Predicate<String> getResponseFilterIgnore() {
        return responseFilterIgnore;
    }

    public void setDescription(@Nonnull ServiceDescription description) {
        this.description = description;
    }

    @Override
    public void setClientFactory(Supplier<Client> factory) {
        this.clientFactory = factory;
    }

    @Override
    public void setResponseFilterIgnore(@Nonnull Predicate<String> predicate) {
        this.responseFilterIgnore = predicate;
    }
}
