package br.ufsc.inf.lapesd.ontogenesis.adapter.enrichers;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.core.MediaType;

import org.apache.commons.lang3.tuple.ImmutablePair;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import br.ufsc.inf.lapesd.ontogenesis.adapter.OntoGenesisAdapter;
import br.ufsc.inf.lapesd.ontogenesis.engine.SemanticFeatures;
import br.ufsc.inf.lapesd.ontogenesis.engine.repmanager.ParsedRepresentation;
import br.ufsc.inf.lapesd.ontogenesis.engine.repmanager.ResourceRepresentation;
import br.ufsc.inf.lapesd.ontogenesis.engine.repmanager.parsers.RepresentationParserJson;

public class JSONLDEnricher {
    public static final MediaType APPLICATION_LDJSON = new MediaType("application", "ld+json");
    private String prefix;
    private Map<String, String> mapSynSem;

    public JSONLDEnricher(OntoGenesisAdapter adapter) {
        SemanticFeatures semanticFeatures = adapter.getSemanticFeatures();
        if (semanticFeatures != null) {
            mapSynSem = semanticFeatures.getMappingSyntSemantic();
            prefix = semanticFeatures.getOntologyPrefix();
        }
    }

    public boolean isEnabled() {
        return mapSynSem != null;
    }

    public void enrich(JsonElement element) {
        if (isEnabled())
            enrich(element, null, false);
    }

    private String abbrev(String uri) {
    	if(uri == null) 
    		return null;
        return uri.startsWith(prefix) ? uri.substring(prefix.length()) : uri;
    }

    private ParsedRepresentation getParsedRepresentation(String json) {
        ResourceRepresentation rRepresentation = new ResourceRepresentation(json, "json");
        RepresentationParserJson parser = new RepresentationParserJson(rRepresentation);
        return parser.getParsedRepresentation();
    }


    private void enrich(JsonElement parsedJson, String typeOverride, boolean inContext) {
        if(parsedJson.isJsonObject()) {
            enrichObject(parsedJson.getAsJsonObject(), typeOverride, inContext);
        } else if (parsedJson.isJsonArray()) {
            enrichArray(parsedJson.getAsJsonArray(), typeOverride, inContext);
        }
    }

    private void enrichObject(JsonObject object, String typeOverride, boolean inContext) {
        if (!inContext) {
            JsonObject context = new JsonObject();
            context.addProperty("@vocab", prefix);
            object.add("@context", context);
        }
        ParsedRepresentation parsed = getParsedRepresentation(object.toString());
        if (typeOverride != null) object.addProperty("@type", typeOverride);
        else if (parsed.getType() != null) object.addProperty("@type", parsed.getType());

        Map<ImmutablePair<String, String>, JsonElement> map = new HashMap<>();
        for (Map.Entry<String, JsonElement> e : object.entrySet()) {
            if (mapSynSem.containsKey(e.getKey())) {
                String sem = abbrev(mapSynSem.get(e.getKey()));
                if(sem != null)
                	map.put(ImmutablePair.of(e.getKey(), sem), e.getValue());
            }
        }
        for (Map.Entry<ImmutablePair<String, String>, JsonElement> e : map.entrySet()) {
            object.remove(e.getKey().left);
            String valueTypes = null;
            if (parsed.getClass(e.getKey().left) != null && e.getValue().isJsonObject())
                valueTypes = parsed.getClass(e.getKey().left).getName();
            enrich(e.getValue(), valueTypes, true);
            object.add(e.getKey().right, e.getValue());
        }
    }

    private void enrichArray(JsonArray array, String typeOverride, boolean inContext) {
        for (JsonElement jsonElement : array)
            enrich(jsonElement, typeOverride, inContext);
    }
}
