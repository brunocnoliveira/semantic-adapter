package br.ufsc.inf.lapesd.ontogenesis.adapter.providers;

import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

import javax.ws.rs.ext.Provider;

import org.glassfish.jersey.server.model.Resource;
import org.glassfish.jersey.server.model.ResourceMethod;
import org.glassfish.jersey.server.model.ResourceModel;
import org.glassfish.jersey.server.monitoring.ApplicationEvent;
import org.glassfish.jersey.server.monitoring.ApplicationEventListener;
import org.glassfish.jersey.server.monitoring.RequestEvent;
import org.glassfish.jersey.server.monitoring.RequestEventListener;

import com.fasterxml.classmate.ResolvedType;
import com.fasterxml.classmate.TypeResolver;

import br.ufsc.inf.lapesd.ontogenesis.adapter.OntoGenesisAdapter;
import br.ufsc.inf.lapesd.ontogenesis.adapter.servicedesc.repository.ServiceDescHydra;

@Provider
public class EndpointsListener implements ApplicationEventListener {

    private static final TypeResolver TYPE_RESOLVER = new TypeResolver();

    private String applicationPath = "";

    private boolean withOptions = false;
    private boolean withWadl = false;
    
    public EndpointsListener() {
    	if(OntoGenesisAdapter.getInstance().getSemanticFeatures() != null) {
	    	String ontologyPrefix = OntoGenesisAdapter.getInstance().getSemanticFeatures().getOntologyPrefix();
	    	applicationPath = OntoGenesisAdapter.getInstance().getConfig().getDescription().getUriBase();
	    	//Removes the /ontology# at the end of string
	    	applicationPath = ontologyPrefix.substring(0,ontologyPrefix.indexOf(applicationPath)) + applicationPath;
    	}
    }

    public EndpointsListener(String applicationPath) {
        this.applicationPath = applicationPath;
    }

    
    @Override
    public void onEvent(ApplicationEvent event) {
        if (event.getType() == ApplicationEvent.Type.INITIALIZATION_FINISHED) {
        	
        	if(!OntoGenesisAdapter.getInstance().isRegistered()) {
        		System.out.println("The semantic service description could not be generated since this service has not been successfully registered in OntoGenesis.");
        		return;
        	}
        	
            final ResourceModel resourceModel = event.getResourceModel();
            final ResourceDetails resourceDetails = new ResourceDetails();
            
            resourceModel.getResources()
            			 .stream()
            			 .forEach((resource) -> 
            			 		 resourceDetails.addEndpointLines(getLinesFromResource(resource)));
            
            resourceDetails.addEndpointsInServiceDescription();
            resourceDetails.print();
        }
    }

    
    @Override
    public RequestEventListener onRequest(RequestEvent requestEvent) {
        return null;
    }

    public EndpointsListener withOptions() {
        this.withOptions = true;
        return this;
    }

    public EndpointsListener withWadl() {
        this.withWadl = true;
        return this;
    }

    private Set<EndpointInfo> getLinesFromResource(Resource resource) {
        Set<EndpointInfo> logLines = new HashSet<>();
        populate(this.applicationPath, false, resource, logLines);
        return logLines;
    }

    private void populate(String basePath, Class<?> klass, boolean isLocator,
            Set<EndpointInfo> endpointLogLines) {
        populate(basePath, isLocator, Resource.from(klass), endpointLogLines);
    }

    private void populate(String basePath, boolean isLocator, Resource resource,
            Set<EndpointInfo> endpointLines) {
        if (!isLocator) {
            basePath = normalizePath(basePath, resource.getPath());
        }

        for (ResourceMethod method : resource.getResourceMethods()) {
            if (!withOptions && method.getHttpMethod().equalsIgnoreCase("OPTIONS")) {
                continue;
            }
            if (!withWadl && basePath.contains(".wadl")) {
                continue;
            }
            endpointLines.add(new EndpointInfo(method.getHttpMethod(), basePath, null));
        }

        for (Resource childResource : resource.getChildResources()) {
            for (ResourceMethod method : childResource.getAllMethods()) {
                if (method.getType() == ResourceMethod.JaxrsType.RESOURCE_METHOD) {
                    final String path = normalizePath(basePath, childResource.getPath());
                    if (!withOptions && method.getHttpMethod().equalsIgnoreCase("OPTIONS")) {
                        continue;
                    }
                    if (!withWadl && path.contains(".wadl")) {
                        continue;
                    }
                    endpointLines.add(new EndpointInfo(method.getHttpMethod(), path, null));
                } 
                else if (method.getType() == ResourceMethod.JaxrsType.SUB_RESOURCE_LOCATOR) {
                    final String path = normalizePath(basePath, childResource.getPath());
                    final ResolvedType responseType = TYPE_RESOLVER
                            .resolve(method.getInvocable().getResponseType());
                    final Class<?> erasedType = !responseType.getTypeBindings().isEmpty()
                            ? responseType.getTypeBindings().getBoundType(0).getErasedType()
                            : responseType.getErasedType();
                    populate(path, erasedType, true, endpointLines);
                }
            }
        }
    }

    private static String normalizePath(String basePath, String path) {
        if (path == null) {
            return basePath;
        }
        if (basePath.endsWith("/")) {
            return path.startsWith("/") ? basePath + path.substring(1) : basePath + path;
        }
        return path.startsWith("/") ? basePath + path : basePath + "/" + path;
    }

    private static class ResourceDetails {

        private static final Comparator<EndpointInfo> COMPARATOR
                = Comparator.comparing((EndpointInfo e) -> e.path)
                .thenComparing((EndpointInfo e) -> e.httpMethod);

        private final Set<EndpointInfo> endpointLines = new TreeSet<>(COMPARATOR);

        private void print() {
            StringBuilder sb = new StringBuilder("--> Initial endpoints registered by Semantic Adapter:\n");
            endpointLines.stream()
            			 .forEach(line -> sb.append(line).append("\n"));
            System.out.println(sb.toString());
        }
        
        public void addEndpointsInServiceDescription() {
        	for(EndpointInfo ep : endpointLines)
        		ServiceDescHydra.getInstance().addEndPoint(ep.httpMethod, ep.path);
        	
        	System.out.println(ServiceDescHydra.getInstance().getDescriptionAsString());
        }

        private void addEndpointLines(Set<EndpointInfo> lines) {
            this.endpointLines.addAll(lines);
        }
    }

    private static class EndpointInfo {

        private static final String DEFAULT_FORMAT = "   %-7s %s";
        final String httpMethod;
        final String path;
        final String format;

        private EndpointInfo(String httpMethod, String path, String format) {
            this.httpMethod = httpMethod;
            this.path = path;
            this.format = format == null ? DEFAULT_FORMAT : format;
        }        

        @Override
        public String toString() {
            return String.format(format, httpMethod, path);
        }
    }
    
}