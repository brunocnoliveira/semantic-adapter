package br.ufsc.inf.lapesd.ontogenesis.adapter.providers;

import br.ufsc.inf.lapesd.ontogenesis.adapter.OntoGenesisAdapter;

import javax.annotation.Nonnull;
import javax.ws.rs.core.Application;
import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.Provider;

/**
 * {@link ContextResolver} that provides a particular {@link OntoGenesisAdapter} instance.
 *
 * This class is not annoted with @{@link Provider}, as it should be added as a singleton to
 * the {@link Application} class.
 */
public class InstanceOntoGenesisAdapterContextResolver implements ContextResolver<OntoGenesisAdapter> {
    private @Nonnull OntoGenesisAdapter adapter;

    public InstanceOntoGenesisAdapterContextResolver(@Nonnull OntoGenesisAdapter adapter) {
        this.adapter = adapter;
    }

    @Override
    public OntoGenesisAdapter getContext(Class<?> type) {
        return adapter;
    }
}
