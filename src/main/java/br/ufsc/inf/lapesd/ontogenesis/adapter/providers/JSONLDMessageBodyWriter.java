package br.ufsc.inf.lapesd.ontogenesis.adapter.providers;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;

import javax.annotation.Nonnull;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyWriter;
import javax.ws.rs.ext.Provider;
import javax.ws.rs.ext.Providers;

import org.apache.commons.io.IOUtils;

import com.google.common.base.Preconditions;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import br.ufsc.inf.lapesd.ontogenesis.adapter.OntoGenesisAdapter;
import br.ufsc.inf.lapesd.ontogenesis.adapter.enrichers.JSONLDEnricher;
import br.ufsc.inf.lapesd.ontogenesis.engine.SemanticFeatures;

@Provider
@Produces("application/ld+json")
public class JSONLDMessageBodyWriter extends OntogenesisProvider implements MessageBodyWriter<Object> {

    public JSONLDMessageBodyWriter() { }


    public JSONLDMessageBodyWriter(@Nonnull OntoGenesisAdapter adapter) {
        super(adapter);
    }

    @Override
    public boolean isWriteable(Class<?> aClass, Type type, Annotation[] annotations,
                               MediaType mediaType) {
        if (!mediaType.isCompatible(JSONLDEnricher.APPLICATION_LDJSON))
            return false; //we produce only JSON-LD
        MessageBodyWriter<?> writer;
        writer = providers.getMessageBodyWriter(aClass, type, annotations,
                MediaType.APPLICATION_JSON_TYPE);
        //but someone has to get us a JSON first!
        return writer != null && getAdapter().getSemanticFeatures() != null;
    }

    @Override
    public long getSize(Object o, Class<?> aClass, Type type, Annotation[] annotations, MediaType mediaType) {
        return -1; //ignored in JAX-RS 2.0
    }

    @Override
    public void writeTo(Object o, Class<?> aClass, Type type, Annotation[] annotations,
                        MediaType mediaType, MultivaluedMap<String, Object> multivaluedMap,
                        OutputStream outputStream) throws IOException, WebApplicationException {

        SemanticFeatures semFeatures = getAdapter().getSemanticFeatures();
        Preconditions.checkArgument(semFeatures != null);
        String json = getJson(o, aClass, type, annotations, multivaluedMap);
        getAdapter().sendToOntoGenesis(json, MediaType.APPLICATION_JSON_TYPE);

        JsonElement element = new JsonParser().parse(json);
        new JSONLDEnricher(getAdapter()).enrich(element);
        IOUtils.write(element.toString(), outputStream, StandardCharsets.UTF_8);
    }


    private String getJson(Object o, Class<?> aClass, Type type, Annotation[] annotations,
                           MultivaluedMap<String, Object> multivaluedMap) throws IOException {
        return getJson(providers, o, aClass, type, annotations, multivaluedMap);
    }

    public static String getJson(Providers providers, Object o, Class<?> aClass, Type type, Annotation[] annotations,
                                 MultivaluedMap<String, Object> multivaluedMap) throws IOException {
        MessageBodyWriter<Object> w;
        //noinspection unchecked
        w = providers.getMessageBodyWriter((Class<Object>) aClass, type, annotations,
                MediaType.APPLICATION_JSON_TYPE);
        Preconditions.checkArgument(w != null);

        ByteArrayOutputStream baOut = new ByteArrayOutputStream();
        w.writeTo(o, aClass, type, annotations, MediaType.APPLICATION_JSON_TYPE,
                  multivaluedMap, baOut);
        return baOut.toString(StandardCharsets.UTF_8.name());
    }
}
