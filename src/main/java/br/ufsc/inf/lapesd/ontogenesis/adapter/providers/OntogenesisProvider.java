package br.ufsc.inf.lapesd.ontogenesis.adapter.providers;

import br.ufsc.inf.lapesd.ontogenesis.adapter.OntoGenesisAdapter;
import br.ufsc.inf.lapesd.ontogenesis.adapter.servicedesc.repository.ServiceDescHydra;

import javax.annotation.Nonnull;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.Providers;

public abstract class OntogenesisProvider {
    @Context
    protected Providers providers;
    private OntoGenesisAdapter overrideAdapter;

    protected OntogenesisProvider() {}

    protected OntogenesisProvider(OntoGenesisAdapter adapter) {
        this.overrideAdapter = adapter;
    }

    @Nonnull
    protected OntoGenesisAdapter getAdapter() {
        if (overrideAdapter != null) return overrideAdapter;
        ContextResolver<OntoGenesisAdapter> resolver;
        resolver = providers.getContextResolver(OntoGenesisAdapter.class, MediaType.WILDCARD_TYPE);
        OntoGenesisAdapter adapter = resolver.getContext(Object.class);
        return adapter == null ? OntoGenesisAdapter.getInstance() : adapter;
    }
    
    protected ServiceDescHydra getServiceDescription() {
    	return ServiceDescHydra.getInstance();
    }
}
