package br.ufsc.inf.lapesd.ontogenesis.adapter.providers;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.net.URI;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.Nonnull;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.core.Link;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.ext.Provider;

import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFLanguages;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import br.ufsc.inf.lapesd.ontogenesis.adapter.OntoGenesisAdapter;
import br.ufsc.inf.lapesd.ontogenesis.adapter.enrichers.JSONLDEnricher;

@Provider
public class OntogenesisResponseFilter extends OntogenesisProvider
        implements ContainerResponseFilter {
	
	private final String SERVICE_DESC_PATH = "serviceDescription";
	private final String HYDRA_CORE_DOC = "http://www.w3.org/ns/hydra/core#apiDocumentation";
	
	private static final Set<String> semanticContentTypes;

    static {
    	semanticContentTypes = RDFLanguages.getRegisteredLanguages().stream()
    			.filter(l -> l != Lang.RDFNULL)
    			.flatMap(l -> l.getAltContentTypes().stream()).collect(Collectors.toSet());
    }
	
    public OntogenesisResponseFilter() {
    }


    public OntogenesisResponseFilter(OntoGenesisAdapter adapter) {
        super(adapter);
    }
    
    

    @Override
    public void filter(ContainerRequestContext requestContext,
                       ContainerResponseContext responseContext) throws IOException {

    	//Include in Response Header the Link of hydra service description
    	addServiceDescLinkInHeader(responseContext);
    	
        String path = requestContext.getUriInfo().getPath();
        if (path.startsWith("/")) 
        	path = path.substring(1);        
        OntoGenesisAdapter adapter = getAdapter();
        if (adapter.getConfig().getResponseFilterIgnore().test(path))
            return;

        MediaType mt = responseContext.getMediaType();
        if(mt == null)
        	return;
        String mtString = mt.getType() + "/" + mt.getSubtype();
        if (semanticContentTypes.contains(mtString))
            return;

        adapter.sendToOntoGenesis(responseContext.getEntity(), mt);

        boolean supportsJSONLD = requestContext.getAcceptableMediaTypes().stream()
                .anyMatch(JSONLDEnricher.APPLICATION_LDJSON::isCompatible);
        
        String json = JSONLDMessageBodyWriter.getJson(providers, responseContext.getEntity(),
                responseContext.getEntityClass(), responseContext.getEntityType(),
                responseContext.getEntityAnnotations(), responseContext.getHeaders());
        
        String newJson = json;
        if (supportsJSONLD) {
        	JsonElement element = new JsonParser().parse(json);
            enrichToJSONLD(element, responseContext, adapter);
            newJson = element.toString();
        }
        
        //Enhance the Semantic Service Description with new ontology concepts.
        enhanceServiceDescription(requestContext, responseContext, newJson);
    }
    

    private void enrichToJSONLD(JsonElement element, ContainerResponseContext responseContext,
                                @Nonnull OntoGenesisAdapter adapter) throws IOException {    	
        new JSONLDEnricher(adapter).enrich(element);
        responseContext.setEntity(element.toString(), new Annotation[0],
                JSONLDEnricher.APPLICATION_LDJSON);
    }
    
    
    private void addServiceDescLinkInHeader(ContainerResponseContext rCtx) {
		String ontologyPrefix = OntoGenesisAdapter.getInstance().getSemanticFeatures().getOntologyPrefix();
		UriBuilder absolute = UriBuilder.fromUri(ontologyPrefix);
	    URI serviceDescUrl = absolute.clone().path(SERVICE_DESC_PATH).build();
	    Link link = Link.fromUri(serviceDescUrl)
	    				.rel(HYDRA_CORE_DOC)
	    				.build();
    	
	    rCtx.getHeaders().add("Link", link);
	}
    
    
    private void enhanceServiceDescription(ContainerRequestContext requestContext, 
    									   ContainerResponseContext responseContext,
    									   String json) {    	    	
    	getServiceDescription().enhanceDescription(requestContext.getMethod(), 
    													  requestContext.getUriInfo(), 
    													  json);
    }

}
