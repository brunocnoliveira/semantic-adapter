package br.ufsc.inf.lapesd.ontogenesis.adapter.providers;

import javax.annotation.Nonnull;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.apache.jena.rdf.model.Model;

import br.ufsc.inf.lapesd.ontogenesis.adapter.OntoGenesisAdapter;
import br.ufsc.inf.lapesd.ontogenesis.adapter.servicedesc.repository.ServiceDescHydra;

public abstract class OntologyEndpoint extends OntogenesisProvider {
    public OntologyEndpoint() { }


    public OntologyEndpoint(@Nonnull OntoGenesisAdapter adapter) {
        super(adapter);
    }

    @GET
    public Model get() {
        return getAdapter().getOntology();
    }
    
    @GET
    @Path("serviceDescription")
    @Produces("application/ld+json")
    public Response getServiceDesc() {
        return Response.ok(ServiceDescHydra.getInstance().getDescriptionAsString()).build();
    }
    
}
