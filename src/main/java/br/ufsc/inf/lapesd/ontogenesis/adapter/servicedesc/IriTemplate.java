package br.ufsc.inf.lapesd.ontogenesis.adapter.servicedesc;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

public class IriTemplate {
		
	@SerializedName("@type")
	private String type = "IriTemplate";
	
	private String template = "";
	
	private List<Mapping> mapping = new ArrayList<Mapping>();
	
	private List<SupportedOperation> supportedOperation = new ArrayList<SupportedOperation>();
	
	
	public IriTemplate() { }
	
	public IriTemplate(String template) {
		setTemplate(template);
	}

	
	public String getType() {
		return type;
	}

	public String getTemplate() {
		return template;
	}

	public void setTemplate(String template) {
		this.template = template;
	}

	public List<Mapping> getMapping() {
		return mapping;
	}

	public void setMapping(List<Mapping> mapping) {
		this.mapping = mapping;
	}

	public List<SupportedOperation> getSupportedOperation() {
		return supportedOperation;
	}

	public void setSupportedOperation(List<SupportedOperation> supportedOperation) {
		this.supportedOperation = supportedOperation;
	}
	
	
	@Override
    public String toString() {
        return new Gson().toJson(this);		
    }

}
