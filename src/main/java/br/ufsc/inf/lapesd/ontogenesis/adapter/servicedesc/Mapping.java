package br.ufsc.inf.lapesd.ontogenesis.adapter.servicedesc;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

public class Mapping {
	
	@SerializedName("@type")
	private String type = "IriTemplateMapping";
	
	private String variable; //variable in the template: e.g. {id}
	
	private String property = "http://www.w3.org/ns/hydra/core#freetextQuery"; //Replaced by the ontology property
	
	private boolean required = true;
	

	public String getType() {
		return type;
	}

	public String getVariable() {
		return variable;
	}

	public void setVariable(String variable) {
		this.variable = variable;
	}

	public String getProperty() {
		return property;
	}

	public void setProperty(String property) {
		this.property = property;
	}

	public boolean getRequired() {
		return required;
	}

	public void setRequired(boolean required) {
		this.required = required;
	}
	
	@Override
    public String toString() {
        return new Gson().toJson(this);
    }
	

}
