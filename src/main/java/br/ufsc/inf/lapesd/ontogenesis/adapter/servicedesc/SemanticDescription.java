package br.ufsc.inf.lapesd.ontogenesis.adapter.servicedesc;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

public class SemanticDescription {
	
	@SerializedName("@context")
	private String context = "http://www.w3.org/ns/hydra/context.jsonld";
	
	private List<IriTemplate> iriTemplate = new ArrayList<IriTemplate>();
	
	
	public List<IriTemplate> getIriTemplate() {
		return this.iriTemplate;
	}
	
	public void setIriTemplate(List<IriTemplate> iriTemplate) {
		this.iriTemplate = iriTemplate;
	}
	
	@Override
    public String toString() {
        return new Gson().toJson(this);
    }

}
