package br.ufsc.inf.lapesd.ontogenesis.adapter.servicedesc;

import javax.ws.rs.HttpMethod;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

public class SupportedOperation {
	
	@SerializedName("@type")
	private String type = "Operation";
	
	private String method = HttpMethod.GET;
	
	private String returns = "";
	
	private String possibleStatus;
	

	public String getType() {
		return type;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getReturns() {
		return returns;
	}

	public void setReturns(String returns) {
		this.returns = returns;
	}

	public String getPossibleStatus() {
		return possibleStatus;
	}

	public void setPossibleStatus(String possibleStatus) {
		this.possibleStatus = possibleStatus;
	}
	
	@Override
    public String toString() {
        return new Gson().toJson(this);
    }

	
}
