package br.ufsc.inf.lapesd.ontogenesis.adapter.servicedesc.repository;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.validation.constraints.NotNull;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

import com.google.gson.Gson;
import com.google.gson.JsonElement;

import br.ufsc.inf.lapesd.ontogenesis.adapter.OntoGenesisAdapter;
import br.ufsc.inf.lapesd.ontogenesis.adapter.servicedesc.IriTemplate;
import br.ufsc.inf.lapesd.ontogenesis.adapter.servicedesc.Mapping;
import br.ufsc.inf.lapesd.ontogenesis.adapter.servicedesc.SemanticDescription;
import br.ufsc.inf.lapesd.ontogenesis.adapter.servicedesc.SupportedOperation;
import br.ufsc.inf.lapesd.ontogenesis.engine.SemanticFeatures;

/**
 * The repository which manages the semantic service description of service.
 * It returns the service description in JSON-LD using Hydra vocabulary.
 * @author Bruno
 *
 */
public class ServiceDescHydra {
	
	private static ServiceDescHydra instance;
	private SemanticDescription templateRep = new SemanticDescription();
	
	
	private ServiceDescHydra() { }
	
	public static ServiceDescHydra getInstance() {
		if(instance == null)
			instance = new ServiceDescHydra();
		return instance;
	}
	
	public String getServiceDescUri() {
		String ontologyPrefix = OntoGenesisAdapter.getInstance().getSemanticFeatures().getOntologyPrefix();
		return (ontologyPrefix != null ? ontologyPrefix.substring(0, ontologyPrefix.length()-1) + "/serviceDescription" : "");
	}
	
	
	public SemanticDescription getTemplate() {
		return templateRep;
	}
	
	public String getDescriptionAsString() {
		return templateRep.toString();
	}
	
	public IriTemplate getIriTemplate(String iriTemplate) {
		return templateRep.getIriTemplate()
						.stream()
						.filter(t -> t.getTemplate().equalsIgnoreCase(iriTemplate))
						.findFirst().orElse(null);
	}
	
	public SupportedOperation getSupportedOperation(String iriTemplate, String method) {
		IriTemplate temp = getIriTemplate(iriTemplate);		
		return temp == null ? null
							: temp.getSupportedOperation()
											.stream()
											.filter(s -> s.getMethod().equalsIgnoreCase(method))
											.findFirst().orElse(null);
	}
	
	public Mapping getMappingVariable(String iriTemplate, String variable) {
		IriTemplate temp = getIriTemplate(iriTemplate);		
		return temp == null ? null
							: temp.getMapping()
									.stream()
									.filter(m -> m.getVariable().equalsIgnoreCase(variable))
									.findFirst().orElse(null);
	}
	
	/**
	 * Add an IriTemplate and returns it
	 * @param httpMethod
	 * @param path
	 * @return the IRI Template that has just been added or the existing IRI Template
	 */
	public IriTemplate addEndPoint(String httpMethod, String path) {		
		path = normalizePathToHydraTempalte(path);
		
		IriTemplate temp = getIriTemplate(path);		
		
		if(temp == null) {
			temp = new IriTemplate(path);
			//Add in the repository
			templateRep.getIriTemplate().add(temp);
		}
		
		//If operation has not already been included, add it
		if(getSupportedOperation(path, httpMethod) == null) {
			SupportedOperation s = new SupportedOperation();
			s.setMethod(httpMethod);
			temp.getSupportedOperation().add(s);
		}

		//For each variable: If it has not already been included, add it
		for(String var : getVariablesFromPath(path)) {
			if(getMappingVariable(path, var) == null) {
				Mapping m = new Mapping();
				m.setVariable(var);
				temp.getMapping().add(m);
			}
		}
		
		return temp;
	}	
	
	
	private String buildUriTemplate(@NotNull UriInfo uriInfo) {
    	String baseUri = uriInfo.getBaseUri().toString();
    	String endOfUri = uriInfo.getRequestUri().toString().replace(baseUri, "");
    	
    	//Path Params
    	Set<String> pathParameters = uriInfo.getPathParameters().keySet();
    	for(String key : pathParameters) {
    		List<String> valuesParam = uriInfo.getPathParameters().get(key);
    		for(String value : valuesParam) {
    			endOfUri = endOfUri.replaceFirst(value, 
    											 String.format("{%s}", key));
    		}
    	}
    	
    	//Query Params
    	Set<String> queryParameters = uriInfo.getQueryParameters().keySet();
    	for(String key : queryParameters) {
    		List<String> valuesParam = uriInfo.getQueryParameters().get(key);
    		for (String value : valuesParam) {
    			endOfUri = endOfUri.replaceFirst(String.format("%s=%s",  key,value), 
    											 String.format("%s={%s}", key,key));
			}
    	}
    	
    	return baseUri.concat(endOfUri);
    }
	
	
	//Finds Matching Pattern (to get variables) in path
    private List<String> getVariablesFromPath(String path) {
    	List<String> vars = new ArrayList<String>();
    	
    	//QueryParams ?var=...&var2=...
    	try {
	    	String[] urlParts = path.split("\\?");
	        if (urlParts.length > 1) {
	            String query = urlParts[1];
	            for (String param : query.split("&")) {
	                String[] pair = param.split("=");
	                if (pair.length > 1) {
	                	String key = URLDecoder.decode(pair[0], "UTF-8");
	                    //String value = URLDecoder.decode(pair[1], "UTF-8");
	                	if(key.startsWith("?")) 
	                		key = key.substring(1);
	                	vars.add(key);
	                }
	            }
	        }
    	} 
    	catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
    	
    	//PathParams /{}
    	Pattern regex = Pattern.compile("\\{(.*?)\\}");
    	Matcher regexMatcher = regex.matcher(path);
    	while (regexMatcher.find()) {
    		String var = regexMatcher.group(1);
    		if(var.startsWith("?"))
    			var = var.substring(1);
    		vars.add(var);
    	}
    	return vars;
    }
    
    
    /** Include the variables in queryParams.  E.g: ?requests=100 -> ?requests={?requests}
     * Puts the "?" after open brackets. E.g: {id} -> {?id}
     * @param path
     * @return
     */
    private String normalizePathToHydraTempalte(String path) {    	
    	try {
	    	String[] urlParts = path.split("\\?");
	        if (urlParts.length > 1) {
	            String query = urlParts[1];
	            String query2 = query;
	            for (String param : query.split("&")) {
	                String[] pair = param.split("=");
	                if (pair.length > 1) {
	                	String key = URLDecoder.decode(pair[0], "UTF-8");
	                    String value = URLDecoder.decode(pair[1], "UTF-8");
	                    query2 = query2.replace(value, String.format("{%s}",key));
	                }
	            }
	            path = path.replace(query, query2);
	        }
    	}
    	catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
    	
    	return path.replaceAll("\\{", "\\{\\?");
    }
    
    
    
    /**
	 * Enhance the semantic service description.
	 * It includes/updates the IRI Template with the concepts defined in SemanticFeatures based on the representation.
	 * It tries to extract the parameters (Mapping) and the returns (SupportedOperation)
	 * @param httpMethod
	 * @param uriInfo
	 * @param json
	 */
	public void enhanceDescription(@NotNull  String httpMethod, @NotNull  UriInfo uriInfo, @NotNull String json) {
		String uriTemplate = buildUriTemplate(uriInfo);
		IriTemplate iriTemplate = addEndPoint(httpMethod, uriTemplate);
		
		Map<String, List<String>> mapJsonToOntoGenesis = new java.util.HashMap<String, List<String>>();	
		Map<String, String> mappingSyntSemantic = OntoGenesisAdapter.getInstance().getSemanticFeatures().getMappingSyntSemantic();
		
		/* ENRICH THE PARAMETERS */
		for(Mapping m : iriTemplate.getMapping()) {
			String prop = mappingSyntSemantic.get(m.getVariable());
			
			/*Matches by property name: If there is a syntactic property (mapped in the semantic mapping)
			 that its name = parameter in the request, then get the ontology property and fill the Mapping of Hydra */
			if(prop != null) {
				m.setProperty(prop);
			}
			else {
				/* Stores the property and values to send to OntoGenesis later */
				List<String> paramValues = getValuesOfParam(uriInfo, m.getVariable());
				mapJsonToOntoGenesis.put(m.getVariable(), paramValues);
			}
		}
		
		/* IF there is a param which has not a property in semantic mapping, then
		 * Calls OntoGenesis passing a Json with the {"parameter": ["values"] ... } */
		if(!mapJsonToOntoGenesis.isEmpty()) {
			SemanticFeatures semanticFeatures = OntoGenesisAdapter.getInstance()
					  							.sendToOntoGenesis(new Gson().toJson(mapJsonToOntoGenesis), 
					  												MediaType.APPLICATION_JSON_TYPE);			
			for(String param : mapJsonToOntoGenesis.keySet()) {
				/*Checks if OntoGenesis could enrich it and get the ontology property in the semantic mapping
				 If it could not enrich, the Hydra mapping remains empty for now. */
				String prop = semanticFeatures.getMappingSyntSemantic().get(param);
				if(prop != null) {
					iriTemplate.getMapping().stream()
								 			.filter(m -> m.getVariable().equals(param))
								 			.forEach(a -> a.setProperty(prop));
				}
			}
		}
		
		/* ENRICH THE RETURNS */
		/* Gets the Operation, which contains the returns of the httpMethod */
		SupportedOperation operation = iriTemplate.getSupportedOperation()
										   		  .stream()
										   		  .filter(sp -> sp.getMethod().equals(httpMethod))
										   		  .findAny().orElse(null);
		/* Enrich the returns of Operation based on the @type of Json */
		if(operation != null) {
			String type = getEntityTypeOfJson(json);
			operation.setReturns(type);
		}
	}
	
	
	private String getEntityTypeOfJson(String json) {
		String type = "";
		JsonElement jsonElement = new Gson().fromJson(json, JsonElement.class);
		if(jsonElement.isJsonObject()) {
			JsonElement element = jsonElement.getAsJsonObject().get("@type");
			if(element != null && element.isJsonPrimitive()) {
				type = element.getAsJsonPrimitive().getAsString();
			}
			
			JsonElement context = jsonElement.getAsJsonObject().get("@context");
			if(context != null) {
				if(context.isJsonPrimitive()) {
					type = context.getAsJsonPrimitive().getAsString() + type;
				}
				if(context.isJsonObject()) {
					JsonElement vocab = context.getAsJsonObject().get("@vocab");
					if(vocab != null && vocab.isJsonPrimitive())
						type = vocab.getAsJsonPrimitive().getAsString() + type;
				}
			}
		}
		return type;
	}
	
	
	private List<String> getValuesOfParam(@NotNull UriInfo uriInfo, String param) {    	
    	List<String> valuesParam = new ArrayList<String>();
    	
    	//Path Params
    	Set<String> pathParameters = uriInfo.getPathParameters().keySet();
    	for(String key : pathParameters) {
    		if(key.equals(param))
    			valuesParam.addAll(uriInfo.getPathParameters().get(key));
    	}
    	
    	//Query Params
    	Set<String> queryParameters = uriInfo.getQueryParameters().keySet();
    	for(String key : queryParameters) {
    		if(key.equals(param)) 
    			valuesParam.addAll(uriInfo.getQueryParameters().get(key));
    	}
    	
    	return valuesParam;
    }

}
