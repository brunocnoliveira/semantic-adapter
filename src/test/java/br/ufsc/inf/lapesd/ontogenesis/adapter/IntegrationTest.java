package br.ufsc.inf.lapesd.ontogenesis.adapter;


import br.ufsc.inf.lapesd.alignator.core.entity.loader.ServiceDescription;
import br.ufsc.inf.lapesd.ld_jaxrs.jena.JenaProviders;
import br.ufsc.inf.lapesd.ontogenesis.adapter.config.MultiPatternPredicate;
import br.ufsc.inf.lapesd.ontogenesis.adapter.config.SimpleOntoGenesisAdapterConfig;
import br.ufsc.inf.lapesd.ontogenesis.adapter.providers.InstanceOntoGenesisAdapterContextResolver;
import br.ufsc.inf.lapesd.ontogenesis.adapter.providers.JSONLDMessageBodyWriter;
import br.ufsc.inf.lapesd.ontogenesis.adapter.providers.OntogenesisResponseFilter;
import br.ufsc.inf.lapesd.ontogenesis.adapter.test.*;
import org.apache.jena.rdf.model.Model;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTestNg;
import org.glassfish.jersey.test.TestProperties;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.ext.ContextResolver;
import java.io.IOException;

import static br.ufsc.inf.lapesd.ontogenesis.adapter.test.Utils.*;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class IntegrationTest extends JerseyTestNg.ContainerPerMethodTest {
    private OntoGenesisMock mock;
    private SimpleOntoGenesisAdapterConfig cfg;
    private OntoGenesisAdapter adapter;
    private ServiceDescription description;

    @Override
    protected Application configure() {
        enable(TestProperties.LOG_TRAFFIC);

        description = new ServiceDescription();
        description.setIpAddress("");
        description.setUriBase("/");
        description.setServerPort("80");

        cfg = new SimpleOntoGenesisAdapterConfig();
        cfg.setOntologyResourcePath("ontology");
        cfg.setDescription(description);
        cfg.setResponseFilterIgnore(new MultiPatternPredicate()
                .addRegex("microservice/.*"));

        adapter = new OntoGenesisAdapter(cfg);
        mock = new OntoGenesisMock();

        ResourceConfig app = new ResourceConfig();
        app.register(new InstanceOntoGenesisAdapterContextResolver(adapter));
        app.register(mock);
        app.register(JSONLDMessageBodyWriter.class);
        app.register(OntogenesisResponseFilter.class);
        app.register(SimpleOntologyEndpoint.class);
        app.register(JsonResourcesEndpoint.class);
        app.register(PersonEndpoint.class);
        app.register(JacksonFeature.class);

        return app;
    }

    @Override
    protected void configureClient(ClientConfig config) {
        JenaProviders.getProviders().forEach(config::register);
        config.register(JacksonFeature.class);
    }

    @BeforeMethod
    @Override
    public void setUp() throws Exception {
        super.setUp();

        String baseURI = getBaseUri().toString();
        cfg.setOntogenesisURI(baseURI);
        cfg.setServiceBaseURI(baseURI);
        mock.semanticFeatures = Utils.semanticFeaturesBuilder()
                .loadOntology("persons/ontology.ttl")
                .rewriteURIs("http://example.org/", baseURI)
                .guessMappings().build();

        if (mock.serviceDescriptions.isEmpty()) {
            adapter.registerService();
            assertEquals(mock.serviceDescriptions.size(), 1);
        }
        mock.representations.clear();
    }

    @Test(priority = -1)
    public void testGetOntology() {
        //service was registered on setUp()
        assertFalse(adapter.getOntology().isEmpty());
    }

    @Test
    public void testSendToOntoGenesis() throws IOException {
        String expected = load("persons/1.json");
        String json = target("/json/persons/1.json").request(MediaType.APPLICATION_JSON_TYPE)
                .get(String.class);
        assertEquals(json, expected);
        assertEquals(mock.representations.size(), 1);
    }

    @Test
    public void testEnrichWithMessageBodyWriter() throws IOException {
        Model expected = rewriteURIs(loadRDF("persons/1.jsonld"),
                "http://example.org/", getBaseUri().toString());
        Model actual = target("/persons/1")
                .request("application/ld+json").get(Model.class);

        assertTrue(actual.isIsomorphicWith(expected));
        assertEquals(mock.representations.size(), 1);
    }

    @Test
    public void testEnrichWithFilter() throws IOException {
        Model expected = rewriteURIs(loadRDF("persons/1.jsonld"),
                "http://example.org/", getBaseUri().toString());
        Model actual = target("/json/persons/1.json")
                .request(MediaType.APPLICATION_JSON, "application/ld+json").get(Model.class);

        assertTrue(actual.isIsomorphicWith(expected));
        assertEquals(mock.representations.size(), 1);
    }
}