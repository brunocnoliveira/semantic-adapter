package br.ufsc.inf.lapesd.ontogenesis.adapter.config;

import org.testng.annotations.Test;

import java.util.regex.PatternSyntaxException;

import static org.testng.Assert.*;

public class MultiPatternPredicateTest {
    @Test
    public void testNone() {
        assertFalse(new MultiPatternPredicate().test(""));
        assertFalse(new MultiPatternPredicate().test("asd"));
        assertFalse(new MultiPatternPredicate().test("false"));
        assertFalse(new MultiPatternPredicate().test("0"));
    }

    @Test(expectedExceptions = {PatternSyntaxException.class})
    public void testBadSyntax() {
        new MultiPatternPredicate().addRegex("(");
    }

    @Test
    public void testSingle() {
        MultiPatternPredicate predicate = new MultiPatternPredicate().addRegex("\\d+");
        assertFalse(predicate.test(""));
        assertFalse(predicate.test("a6"));
        assertFalse(predicate.test("6a"));
        assertTrue(predicate.test("666"));
    }

    @Test(expectedExceptions = {PatternSyntaxException.class})
    public void testBadSyntaxOnSecond() {
        new MultiPatternPredicate().addRegex("asd").addRegex("(");
    }


    @Test
    public void testDouble() {
        MultiPatternPredicate predicate = new MultiPatternPredicate()
                .addRegex("\\d+").addRegex("a\\d+");
        assertFalse(predicate.test(""));
        assertTrue(predicate.test("a6"));
        assertFalse(predicate.test("6a"));
        assertTrue(predicate.test("666"));
    }
}