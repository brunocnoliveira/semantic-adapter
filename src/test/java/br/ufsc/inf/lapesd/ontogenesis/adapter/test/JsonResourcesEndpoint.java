package br.ufsc.inf.lapesd.ontogenesis.adapter.test;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;

@Path("json")
public class JsonResourcesEndpoint {
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{path:.+}")
    public Response get(@PathParam("path") String path) {
        try {
            return Response.ok(Utils.load(path), MediaType.APPLICATION_JSON_TYPE).build();
        } catch (IOException e) {
            throw new WebApplicationException(e, Response.Status.NOT_FOUND);
        }
    }
}
