package br.ufsc.inf.lapesd.ontogenesis.adapter.test;


import br.ufsc.inf.lapesd.alignator.core.entity.loader.ServiceDescription;
import br.ufsc.inf.lapesd.ontogenesis.engine.SemanticFeatures;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;

@Path("microservice")
public class OntoGenesisMock {
    public SemanticFeatures semanticFeatures = new SemanticFeatures();
    public List<ServiceDescription> serviceDescriptions = new ArrayList<>();
    public List<String> representations = new ArrayList<>();

    @Path("description")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public SemanticFeatures registerMicroservice(ServiceDescription description) {
        serviceDescriptions.add(description);
        semanticFeatures.setOntologyPrefix("http://localhost:9998/ontology#");
        return semanticFeatures;
    }

    @Path("representation")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public SemanticFeatures representation(String json) {
        representations.add(json);
        semanticFeatures.setOntologyPrefix("http://localhost:9998/ontology#");
        return semanticFeatures;
    }
}
