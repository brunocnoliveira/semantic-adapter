package br.ufsc.inf.lapesd.ontogenesis.adapter.test;

public class Paper {
    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
