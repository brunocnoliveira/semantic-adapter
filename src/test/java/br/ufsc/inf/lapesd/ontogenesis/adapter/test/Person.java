package br.ufsc.inf.lapesd.ontogenesis.adapter.test;

public class Person {
    private String name;
    private Paper paper;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Paper getPaper() {
        return paper;
    }

    public void setPaper(Paper paper) {
        this.paper = paper;
    }
}
