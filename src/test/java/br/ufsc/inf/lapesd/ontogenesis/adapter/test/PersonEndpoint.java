package br.ufsc.inf.lapesd.ontogenesis.adapter.test;

import com.google.gson.Gson;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import java.io.IOException;

@Path("persons")
public class PersonEndpoint {
    @GET
    @Path("{id}")
    public Person get(@PathParam("id") String id) {
        try {
            return new Gson().fromJson(Utils.load("persons/" + id + ".json"), Person.class);
        } catch (IOException e) {
            throw new WebApplicationException(e, Response.Status.NOT_FOUND);
        }
    }
}
