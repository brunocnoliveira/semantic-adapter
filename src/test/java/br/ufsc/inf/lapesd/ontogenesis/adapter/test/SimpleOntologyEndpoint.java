package br.ufsc.inf.lapesd.ontogenesis.adapter.test;

import br.ufsc.inf.lapesd.ontogenesis.adapter.OntoGenesisAdapter;
import br.ufsc.inf.lapesd.ontogenesis.adapter.providers.OntologyEndpoint;

import javax.annotation.Nonnull;
import javax.ws.rs.Path;

@Path("ontology")
public class SimpleOntologyEndpoint extends OntologyEndpoint {
    public SimpleOntologyEndpoint(@Nonnull OntoGenesisAdapter ontoGenesisAdapter) {
        super(ontoGenesisAdapter);
    }
}
