package br.ufsc.inf.lapesd.ontogenesis.adapter.test;

import br.ufsc.inf.lapesd.ontogenesis.engine.SemanticFeatures;
import com.google.common.base.Preconditions;
import org.apache.commons.io.IOUtils;
import org.apache.jena.rdf.model.*;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFLanguages;
import org.apache.jena.vocabulary.OWL2;
import org.apache.jena.vocabulary.RDF;

import javax.ws.rs.core.MediaType;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utils {
    public static final MediaType APPLICATION_LDJSON = new MediaType("application", "ld+json");

    public static String load(String path) throws IOException {
        try (InputStream in = Utils.class.getClassLoader().getResourceAsStream(path)) {
            return IOUtils.toString(in, StandardCharsets.UTF_8);
        }
    }

    public static Model loadRDF(String path) throws IOException {
        ClassLoader loader = Utils.class.getClassLoader();
        Lang lang = RDFLanguages.filenameToLang(path);
        Model model = ModelFactory.createDefaultModel();
        try (InputStream in = loader.getResourceAsStream(path)) {
            RDFDataMgr.read(model, in, lang);
        }
        return model;
    }

    public static String toString(Model model, Lang lang) {
        try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
            RDFDataMgr.write(out, model, lang);
            return out.toString(StandardCharsets.UTF_8.name());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static Model rewriteURIs(Model model, String regex, String replacement) {
        StmtIterator it = model.listStatements();
        List<Statement> rewritten = new ArrayList<>();
        while (it.hasNext()) {
            Statement s = it.next();
            Statement r = ResourceFactory.createStatement(
                    rewrite(regex, replacement, s.getSubject()).asResource(),
                    rewrite(regex, replacement, s.getPredicate()).as(Property.class),
                    rewrite(regex, replacement, s.getObject()));
            if (!s.equals(r)) {
                rewritten.add(r);
                it.remove();
            }
        }
        rewritten.forEach(model::add);
        return model;
    }

    public static RDFNode rewrite(String regex, String replacement, RDFNode node) {
        Preconditions.checkArgument(node.getModel() != null);
        if (!node.isURIResource()) return node;
        String uri = node.asResource().getURI();
        String replaced = uri.replaceAll(regex, replacement);
        return uri.equals(replaced) ? node : node.getModel().createResource(replaced);
    }

    public static SemanticFeaturesBuilder semanticFeaturesBuilder() {
        return new SemanticFeaturesBuilder();
    }

    public static class SemanticFeaturesBuilder {
        private SemanticFeatures sf;
        private Model ontology;

        public SemanticFeaturesBuilder() {
            reset();
        }

        private void reset() {
            sf = new SemanticFeatures();
            sf.setMappingSyntSemantic(new HashMap<>());
            sf.setEquivalentProperties(new ArrayList<>());
        }

        public SemanticFeaturesBuilder loadOntology(String path) throws IOException {
            ontology = loadRDF(path);
            return this;
        }

        public SemanticFeaturesBuilder rewriteURIs(String regex, String replacement) {
            Utils.rewriteURIs(ontology, regex, replacement);
            return this;
        }

        public SemanticFeaturesBuilder guessMappings() {
            String prefix = ontology.listSubjectsWithProperty(RDF.type, OWL2.Ontology).toList()
                    .stream().filter(RDFNode::isURIResource).map(Resource::getURI)
                    .map(u -> u + "#").findFirst().orElse(null);
            if (prefix == null) return this;

            ontology.listSubjects().toList().stream().filter(RDFNode::isURIResource)
                    .map(Resource::getURI).filter(u -> u.startsWith(prefix)).forEach(u -> {
                String syn = u.substring(prefix.length());
                sf.getMappingSyntSemantic().put(syn, u);
            });
            return this;
        }

        public SemanticFeaturesBuilder addMapping(String syn, String sem) {
            sf.getMappingSyntSemantic().put(syn, sem);
            return this;
        }

        public SemanticFeatures build() {
            sf.setOntologyAsString(Utils.toString(ontology, Lang.RDFXML));
            byte[] bytes = sf.getOntologyAsString().getBytes(StandardCharsets.UTF_8);
            sf.setOntologyBase64(Base64.getEncoder().encodeToString(bytes));
            SemanticFeatures built = this.sf;
            reset();
            return built;
        }
    }
}
